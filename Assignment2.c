#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int data;
    struct Node *next;
};

// Function to create a new node
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode==NULL){
		printf("Error: unable to allocate memory for a new node\n");
		exit(1);
	}
	
        newNode->data = num;
        newNode->next = NULL;
		return newNode;
}
//Function to put new value at the start of list.
void prepend(struct Node **head,int num){
	struct Node *newNode=createNode(num);
	newNode->next=*head;
	*head=newNode;
}

// Function to append a new node to the end of the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
		return;
    }
   else
    {
        struct Node *current = *head;
        while (current->next != NULL)
        {
            current = current->next;
        }
        current->next = newNode;
    }
}

// Function to delete a node with a specific key
void deleteByKey(struct Node **head, int key)
{
    struct Node *temp = *head;
    struct Node *prev = NULL;
    while (temp != NULL && temp->data != key)
    {
        prev = temp;
        temp = temp->next;
    }
    if (temp != NULL)
    {
        if (prev == NULL)
        {
            // Node to be deleted is the head
            *head = temp->next;
        }
        else
        {
            prev->next = temp->next;
        }
        free(temp); // Free memory
    }
}

//Function to delete a node with a specfic value.
void deleteByValue(struct Node **head, int value)
{
    struct Node *temp = *head;
    struct Node *prev = NULL;
    while (temp != NULL && temp->data != value)
    {
        prev = temp;
        temp = temp->next;
    }
    if (temp != NULL)
    {
        if (prev == NULL)
        {
            // Node to be deleted is the head
            *head = temp->next;
        }
        else
        {
            prev->next = temp->next;
        }
        free(temp); // Free memory
        printf("Node with value %d deleted successfully.\n", value);
    }
}


// Function to insert a new node after a specific key
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *newNode = createNode(value);
    struct Node *temp = *head;
    while (temp != NULL && temp->data != key)
    {
        temp = temp->next;
    }
    if (temp != NULL)
    {
        newNode->next = temp->next;
        temp->next = newNode;
    }
}

//Function to insert a new node by a specific value.
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *newNode = createNode(newValue);
    struct Node *temp = *head;
    while (temp != NULL && temp->data != searchValue)
    {
        temp = temp->next;
    }
    if (temp != NULL)
    {
        newNode->next = temp->next;
        temp->next = newNode;
    }
}

//Function to print the linked list
void printList(struct Node *head)
{
    struct Node *current = head;
    printf("[");
    while (current != NULL)
    {
        printf("%d", current->data);
        current = current->next;
		
		if(current != NULL){
		printf(",");
		}
	}
    printf("]\n");
}

int main()
{
    struct Node *head = NULL;
    int choice, data;

    while (1)
    {
      printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by key\n");
		printf("5. Delete by value\n");
		printf("6. insert by key\n");
		printf("7. insert by value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        if (choice == 1)
        {
            printList(head);
            
        }
        else if (choice == 2)
        {
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            
        }
		else if (choice == 3)
        {
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
			
        }
		else if (choice == 4)
        {
            int key;
            printf("Enter key to delete: ");
            scanf("%d", &key);
            deleteByKey(&head, key);
        }
		
		 else if (choice == 5)
        {
            int value;
            printf("Enter value to delete: ");
            scanf("%d", &value);
            deleteByValue(&head, value);
        }
		
		else if (choice == 6)
        {
            int key, value;
            printf("Enter key after which to insert: ");
            scanf("%d", &key);
            printf("Enter value to insert: ");
            scanf("%d", &value);
            insertAfterKey(&head, key, value);
        }
		else if (choice == 7)
        {
            int searchValue, newValue;
            printf("Enter value after which to insert: ");
            scanf("%d", &searchValue);
            printf("Enter new value to insert: ");
            scanf("%d", &newValue);
            insertAfterValue(&head, searchValue, newValue);
        }
		else if(choice==8){
			break;
		}
		else
		{	printf("invalid choice, try again\n");
			break;
		}
    }
    return 0;
}

